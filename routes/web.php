<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{slugtamu}', function(){ 
	return view('welcome');
});

Route::get('/alpha/login', 'Auth\LoginController@showLoginForm');
Route::post('/alpha/login', 'Auth\LoginController@login');
Route::post('/alpha/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('WiFvLMfuPt2t2R47bKsCDZP7ZwCJgRfzGwraTGUso5k47Ih1MphT2xHzWi8R/{slugtamu}', 'Frontend\TemplateController@index');

// Route::get('show/{slugmempelai}/{slugtamu}', 'MainController@showInvitation')->name('showinvitation');
// Route::get('main/{slugmempelai}/{slugtamu}/{call_type}', 'MainController@show')->name('main');

Auth::routes();


Route::group(['middleware' => 'auth', 'prefix' => 'alpha'], function(){
	Route::get('cache', function(){
		Artisan::call('config:cache');
	});

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('list-data', 'Backend\BrideAndGroomController@index')->name('index');
	Route::get('create-data', 'Backend\BrideAndGroomController@create')->name('create');
	Route::post('store-data', 'Backend\BrideAndGroomController@store')->name('store');
	Route::get('show/{slug}', 'Backend\BrideAndGroomController@show')->name('show');
	Route::get('edit/{id}', 'Backend\BrideAndGroomController@edit')->name('edit');
	Route::post('update/{id}', 'Backend\BrideAndGroomController@update')->name('update');
	Route::get('destroy/{id}', 'Backend\BrideAndGroomController@destroy')->name('destroy');
	
	Route::post('store/{id}/{slug}', 'Backend\BrideAndGroomController@storeGuest')->name('store.guest');
	Route::post('export-excel/{id}/{slug}', 'Backend\BrideAndGroomController@exportExcel')->name('exportExcel');
	Route::post('update-guest/{id}/{slug}', 'Backend\BrideAndGroomController@updateGuest')->name('update.guest');
	Route::get('destroy/guest/{id}', 'Backend\BrideAndGroomController@destroyGuest')->name('destroy.guest');

	Route::get('show-report/{slug}', 'Backend\BrideAndGroomController@showReport')->name('show.report');
});