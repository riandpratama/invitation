<?php

namespace App\Imports;

use App\Models\Guest;
use Maatwebsite\Excel\Concerns\ToModel;

class GuestsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Guest([
            'bride_and_groom_id'    => request()->segment(3),
            'name'                  => $row[0], 
            'description'           => $row[1],
            'slug_guest'            => \Str::slug(request()->segment(4).'-'.$row[0]),
            'reference_guest'       => \Str::random(5)
        ]);
    }
}
