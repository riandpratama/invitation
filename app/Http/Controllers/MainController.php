<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slugmempelai, $slugtamu)
    {
        return view('welcome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function showInvitation($slugmempelai, $slugtamu)
    {
        $tamu = $slugmempelai;

        return view('show', compact('tamu'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slugmempelai, $slugtamu, $call_type)
    {
        if(isset($call_type))
        {
            $call_type = $call_type;

            if($call_type == "jquery")
            {
                echo json_encode(array(
                    'status'=>'success',
                    'title'=> 'jQuery Page',
                    'description' => 'jQuery description',
                    'url' => '',
                    'data'=>'This is <strong>jQuery</strong> data coming from ajax url'
                ));
            }
            else if($call_type == "php")
            {
                echo json_encode(array(
                    'status'=>'success',
                    'title'=> 'PHP Page',
                    'description' => 'PHP description',
                    'url' => 'php/'.$call_type.'..php',
                    'data'=>'This is <strong>PHP</strong> data coming from ajax url'
                ));
            }
            else if($call_type == "home")
            {
                echo json_encode(array(
                    'status'=>'success',
                    'title'=> 'Home Page',
                    'description' => 'Home description',
                    'url' => '',
                    'data'=>'This is <strong>Home</strong> data coming from ajax url'
                ));
            }
            else if($call_type == "invoice")
            {
                echo json_encode(array(
                    'status'=>'success',
                    'title'=> 'Invoice receipt Page',
                    'description' => 'Invoice receipt description',
                    'url' => 'invoice/'.$call_type.'..php',
                    'data'=>file_get_contents('invoice-2.html'),
                ));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
