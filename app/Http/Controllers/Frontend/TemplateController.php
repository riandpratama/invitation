<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BrideAndGroom;
use App\Models\Guest;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slugtamu)
    {
        $data = Guest::with('brideAndGroom')->where('slug_guest', $slugtamu)->first();

        if (is_null($data)){
            echo "Not found";
        } else {
            $data = Guest::with('brideAndGroom')->where('slug_guest', $slugtamu)->first();

            if ($data->brideAndGroom->template == 1) {
                return view('frontend.template-one.index', compact('data'));
            } elseif ($data->brideAndGroom->template == 2) {
                return view('frontend.template-two.index', compact('data'));
            } elseif ($data->brideAndGroom->template == 3) {
                return view('frontend.template-three.index', compact('data'));
            }else {
                echo "Not found";
            }
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
