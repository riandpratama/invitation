<?php

namespace App\Http\Controllers\Backend;

use App\Models\Guest;
use App\Models\BrideAndGroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GuestsImport;
use Session;

class BrideAndGroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BrideAndGroom::orderBy('created_at', 'desc')->get();

        return view('backend.bride-and-groom.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.bride-and-groom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        BrideAndGroom::create([
            'groom' => $request->groom,
            'full_name_groom' => $request->full_name_groom,
            'bride' => $request->bride,
            'full_name_bride' => $request->full_name_bride,
            'date' => $request->date,
            'address' => $request->address,
            'district' => $request->district,
            'city' => $request->city,
            'province' => $request->province,
            'event_house' => $request->event_house,
            'the_son_of_the_groom' => $request->the_son_of_the_groom,
            'the_father_of_the_groom' => $request->the_father_of_the_groom,
            'the_mother_of_the_groom' => $request->the_mother_of_the_groom,
            'the_daughter_of_the_bride' => $request->the_daughter_of_the_bride,
            'the_father_of_the_bride' => $request->the_father_of_the_bride,
            'the_mother_of_the_bride' => $request->the_mother_of_the_bride,
            'name_of_the_day' => $request->name_of_the_day,
            'name_of_the_month' => $request->name_of_the_month,
            'year' => $request->year,
            'time' => $request->time,
            'count_down' => $request->count_down,
            'lat' => $request->latitude,
            'long' => $request->longitude,
            'long' => $request->longitude,
            'template' => $request->template,

            'slug_bride_and_groom' => \Str::slug($request->groom).'-'.\Str::slug($request->bride),
            'reference_bride_and_groom' => \Str::random(5)
        ]);

        Session::flash('status', 'Berhasil Menyimpan Data.');

        return redirect()->route('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGuest(Request $request, $id, $slug)
    {
        Guest::create([
            'bride_and_groom_id'    => $id,
            'name'                  => $request->name,
            'description'           => $request->description,
            'slug_guest'            => \Str::slug($slug.'-'.$request->name),
            'reference_guest'       => \Str::random(5)
        ]);

        Session::flash('status', 'Berhasil Menyimpan Data.');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = BrideAndGroom::where('slug_bride_and_groom', $slug)->first();

        return view('backend.bride-and-groom.show', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showReport($slug)
    {
        $data = BrideAndGroom::where('slug_bride_and_groom', $slug)->first();

        return view('backend.bride-and-groom.report', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportExcel(Request $request, $id, $slug)
    {
        if ($request->hasFile('excel')) {
            $file = $request->file('excel');
            Excel::import(new GuestsImport, $file);
        }

        Session::flash('status', 'Berhasil Menyimpan Data.');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = BrideAndGroom::findOrFail($id);

        return view('backend.bride-and-groom.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = BrideAndGroom::findOrFail($id);

        $data->update([
            'groom' => $request->groom,
            'full_name_groom' => $request->full_name_groom,
            'bride' => $request->bride,
            'full_name_bride' => $request->full_name_bride,
            'date' => $request->date,
            'address' => $request->address,
            'district' => $request->district,
            'city' => $request->city,
            'province' => $request->province,
            'event_house' => $request->event_house,
            'the_son_of_the_groom' => $request->the_son_of_the_groom,
            'the_father_of_the_groom' => $request->the_father_of_the_groom,
            'the_mother_of_the_groom' => $request->the_mother_of_the_groom,
            'the_daughter_of_the_bride' => $request->the_daughter_of_the_bride,
            'the_father_of_the_bride' => $request->the_father_of_the_bride,
            'the_mother_of_the_bride' => $request->the_mother_of_the_bride,
            'name_of_the_day' => $request->name_of_the_day,
            'name_of_the_month' => $request->name_of_the_month,
            'year' => $request->year,
            'time' => $request->time,
            'count_down' => $request->count_down,
            'lat' => $request->latitude,
            'long' => $request->longitude,
            'long' => $request->longitude,
            'template' => $request->template,

            'slug_bride_and_groom' => \Str::slug($request->groom).'-'.\Str::slug($request->bride),
            'reference_bride_and_groom' => \Str::random(5)
        ]);

        Session::flash('status', 'Berhasil Menyimpan Data.');

        return redirect()->route('index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGuest(Request $request, $id)
    {
        $data = Guest::findOrFail($id);

        $data->update([
            'name'          => $request->name,
            'description'   => $request->description,
            'slug_guest'    => \Str::slug(request()->segment(4).'-'.$request->name),
        ]);

        Session::flash('status', 'Berhasil Menyimpan Data.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BrideAndGroom::findOrFail($id);

        $dataId = $data->id;

        Guest::where('bride_and_groom_id', $dataId)->delete();
        
        $data->delete();

        Session::flash('status', 'Berhasil Menghapus Data.');

        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyGuest($id)
    {
        $data = Guest::findOrFail($id);

        $dataId = $data->id;

        Guest::where('id', $dataId)->delete();

        Session::flash('status', 'Berhasil Menghapus Data.');

        return redirect()->back();
    }
}
