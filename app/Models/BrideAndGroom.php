<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrideAndGroom extends Model
{
    protected $table = 'bride_and_groom';
    
    protected $guarded = [];

    public function guest()
    {
    	return $this->hasMany(Guest::class);
    }
}
