<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = 'guests';
    
    protected $guarded = [];

    public function brideAndGroom()
    {
    	return $this->belongsTo(BrideAndGroom::class)->withDefault();
    }
}
