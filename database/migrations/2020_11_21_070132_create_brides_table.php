<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBridesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bride_and_groom', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('groom');
            $table->string('full_name_groom');
            $table->string('bride');
            $table->string('full_name_bride');
            $table->string('slug_bride_and_groom');
            $table->string('reference_bride_and_groom');
            $table->date('date');
            $table->text('address');
            $table->text('district');
            $table->text('city');
            $table->text('province');
            $table->text('event_house');

            $table->text('the_son_of_the_groom');
            $table->text('the_father_of_the_groom');
            $table->text('the_mother_of_the_groom');

            $table->text('the_daughter_of_the_bride');
            $table->text('the_father_of_the_bride');
            $table->text('the_mother_of_the_bride');

            $table->text('name_of_the_day');
            $table->text('name_of_the_month');
            $table->text('year');
            $table->text('time');
            $table->dateTime('count_down', 0);
            $table->text('lat');
            $table->text('long');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brides');
    }
}
