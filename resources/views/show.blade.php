<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!-- Styles -->
        <style>
            /*RESET STYLES*/
        *{
           margin: 0;
           padding: 0;
           box-sizing: border-box;
           list-style-type: none;
           font-family: sans-serif;
        }

        .app-container{
          /* The app can only be the height of the viewport */
          display: flex;
          flex-direction: column;
          height: 100vh;
          /* Do NOT allow the app to scroll off the screen */
          overflow: hidden;
        }

        /* HEADER STYLES */
        header{
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          align-items: center;
          padding: 20px;
          /* Give the header a subtle drop shadow */
          box-shadow: 0px 0px 5px rgba(0,0,0,0.2)
        }
        header h1{
          font-size: 1.2em;
        }
        header img{
          width: 40px;
          height: 40px;
          /*  Rounded corners  */
          border-radius: 100%;
        /*   margin: 10px */
        }

        .container {
            background: #f0f0f0;
            position: relative;
            overflow: hidden;
            width: 375px;
            height: 600px;
            margin: 50px auto 0;
            box-shadow: 0 0 50px 10px #aaa;
        }

        main{
          /* Take up all free space on the screen between header and footer */
          flex: 1 1 auto;
          overflow-y: scroll;
        }

        /* FOOTER STYLES */
        footer{
          /* Give the footer a shadow too */
          box-shadow: 0px 0px 5px rgba(0,0,0,0.2)
        }

        footer nav ul{
          display: flex;
          /*  Put everything on one line  */
          flex-direction: row;
          /* Equally space everything  */
          justify-content: space-around;
          align-items: center;
        }

        footer nav ul li{
          /* Stop everything from hitting the edges of the footer   */
          padding: 10px;
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        /* THIS IS JUST FOR DEMO PURPOSES - FORCING THE CENTRAL CONTAINER TO BE LARGER THAN THE SCREEN */
        .spacer{
          min-height: 1000px;
        }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="app-container">
                  <!-- A fixed header, using flexbox -->
                  <header>
                    <h1>My App's name</h1>
                    <img src="http://placehold.it/80" />
                  </header>
                  <!-- The scrollable main view container -->

                  <main>
                    <!--  THIS ELEMENT IS JUST FOR DEMO PURPOSES  -->
                    <div class="spacer">
                        Undangan kepada: {{ $tamu }}
                        <span class="post_msg container"></span>
                    </div>
                  </main>

                  <!-- A footer fixed to the bottom of the screen -->
                  <footer>
                    <nav>
                      <ul>
                        <span class="btn btn-info btn_load_screen" call_type="home">
                        <li>
                          <i class="fa fa-home"></i>
                          <h5> Home</h5>
                        </li>
                        </span>

                        <span class="btn btn-secondary btn_load_screen" call_type="jquery">
                        <li>
                          <i class="fa fa-cog"></i>
                          <h5> Sampul</h5>
                        </li>
                        </span>

                        <span class="btn btn-dark btn_load_screen" call_type="php">
                            <li>
                              <i class="fa fa-list"></i>
                              <h5> Mempelai</h5>
                            </li>
                        </span>

                        <li>
                          <i class="fa fa-user"></i>
                          <h5>Profile</h5>
                        </li>

                        <li>
                          <i class="fa fa-edit"></i>
                          <h5>Profile</h5>
                        </li>

                      </ul>
                    </nav>
                  </footer>
                </div>
            </div>
        </div>
    </body>
    @php
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
          $ssl = 'https';
        }
        else {
          $ssl = 'http';
        }

        $app_url = ($ssl  )
                  . "://".$_SERVER['HTTP_HOST']
                  . (dirname($_SERVER["SCRIPT_NAME"]) == DIRECTORY_SEPARATOR ? "" : "/")
                  . trim(str_replace("\\", "/", dirname($_SERVER["SCRIPT_NAME"])), "/");

    @endphp

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function($)
    {
        var page_url = window.location.origin;
        // console.log(page_url);

        $(document).on('click', '.btn_load_home', function(event)
        {
            event.preventDefault();

            $(document).attr("title", 'Home');
            $(document).find('meta[name=description]').attr('content', data.description);

            window.history.pushState("", "", page_url);
            $(document).find('.post_msg').html(" ");
        });

        $(document).on('click', '.btn_load_screen', function(event)
        {
            event.preventDefault();

            var call_type = $(this).attr('call_type');

            $.getJSON(page_url + '/main/wahyu-rini/riand-pratama/' + call_type, function(data, textStatus, xhr)
            {
                console.log(data);

                $(document).attr("title", data.title);
                $(document).find('meta[name=description]').attr('content', data.description);

                $(document).find('.post_msg').html(data.data);

                window.history.pushState("", "", page_url+data.url);
            });
        });


    });
    </script>
</html>
