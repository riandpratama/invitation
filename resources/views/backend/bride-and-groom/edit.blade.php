@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('update', $data->id) }}">
                        @csrf
                        <div class="form-group">
                            <label for="groom">Groom</label>
                            <input type="text" class="form-control" name="groom" value="{{ $data->groom }}">
                        </div>
                        <div class="form-group">
                            <label for="full_name_groom">Full Name Groom</label>
                            <input type="text" class="form-control" name="full_name_groom" value="{{ $data->full_name_groom }}">
                        </div>
                        <div class="form-group">
                            <label for="bride">Bride</label>
                            <input type="text" class="form-control" name="bride" value="{{ $data->bride }}">
                        </div>
                        <div class="form-group">
                            <label for="full_name_bride">Full Name Bride</label>
                            <input type="text" class="form-control" name="full_name_bride" value="{{ $data->full_name_bride }}">
                        </div>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" class="form-control" name="date" value="{{ $data->date }}">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" value="{{ $data->address }}">
                        </div>
                        <div class="form-group">
                            <label for="district">District</label>
                            <input type="text" class="form-control" name="district" value="{{ $data->district }}">
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" name="city" value="{{ $data->city }}">
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <input type="text" class="form-control" name="province" value="{{ $data->province }}">
                        </div>
                        <div class="form-group">
                            <label for="event_house">Event House</label>
                            <input type="text" class="form-control" name="event_house" value="{{ $data->event_house }}">
                        </div>
                        <div class="form-group">
                            <label for="the_son_of_the_groom">The son of the groom</label>
                            <input type="text" class="form-control" name="the_son_of_the_groom" value="{{ $data->the_son_of_the_groom }}">
                        </div>
                        <div class="form-group">
                            <label for="the_father_of_the_groom">The father of the groom</label>
                            <input type="text" class="form-control" name="the_father_of_the_groom" value="{{ $data->the_father_of_the_groom }}">
                        </div>
                        <div class="form-group">
                            <label for="the_mother_of_the_groom">The mother of the groom</label>
                            <input type="text" class="form-control" name="the_mother_of_the_groom" value="{{ $data->the_mother_of_the_groom }}">
                        </div>
                        <div class="form-group">
                            <label for="the_daughter_of_the_bride">The daughter of the bride</label>
                            <input type="text" class="form-control" name="the_daughter_of_the_bride" value="{{ $data->the_daughter_of_the_bride }}">
                        </div>
                        <div class="form-group">
                            <label for="the_father_of_the_bride">The father of the bride</label>
                            <input type="text" class="form-control" name="the_father_of_the_bride" value="{{ $data->the_father_of_the_bride }}">
                        </div>
                        <div class="form-group">
                            <label for="the_mother_of_the_bride">The mother of the bride</label>
                            <input type="text" class="form-control" name="the_mother_of_the_bride" value="{{ $data->the_mother_of_the_bride }}">
                        </div>
                        <div class="form-group">
                            <label for="name_of_the_day">Name of the day</label>
                            <input type="text" class="form-control" name="name_of_the_day" value="{{ $data->name_of_the_day }}">
                        </div>
                        <div class="form-group">
                            <label for="name_of_the_month">Name of the month</label>
                            <input type="text" class="form-control" name="name_of_the_month" value="{{ $data->name_of_the_month }}">
                        </div>
                        <div class="form-group">
                            <label for="year">Year</label>
                            <input type="text" class="form-control" name="year" value="{{ $data->year }}">
                        </div>
                        <div class="form-group">
                            <label for="time">Time</label>
                            <input type="text" class="form-control" name="time" value="{{ $data->time }}">
                        </div>
                        <div class="form-group">
                            <label for="count_down">Time and day count down</label>
                            <input type="datetime-local" class="form-control" name="count_down" value="{{ date('Y-m-d\TH:i', strtotime($data->count_down))  }}">
                        </div>
                        <div class="form-group">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control" name="latitude" value="{{ $data->lat }}">
                        </div>
                        <div class="form-group">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control" name="longitude" value="{{ $data->long }}">
                        </div>
                        <div class="form-group">
                            <label for="template">Template</label>
                            <select name="template" id="template" class="form-control">
                                <option value="1" @if($data->template == 1) selected @endif>Template One</option>
                                <option value="2" @if($data->template == 2) selected @endif>Template Two</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('index') }}" class="btn btn-danger">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
