@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('create') }}" class="btn btn-primary">Create</a>

                    <table class="table" style="text-align: center;">
                        <tr>
                            <td>No</td>
                            <td>Groom</td>
                            <td>Bride</td>
                            <td>Date</td>
                            <td>Guest</td>
                        </tr>
                        @foreach ($data as $item)
                        <tr>
                            <td>{{ $loop->iteration }}.</td>
                            <td>{{ $item->groom }}</td>
                            <td>{{ $item->bride }}</td>
                            <td>{{ date('d/m/Y', strtotime($item->date)) }}</td>
                            <td>
                                <a href="{{ route('show', $item->slug_bride_and_groom) }}" class="btn btn-success btn-sm">Detail</a>
                                <a href="{{ route('show.report', $item->slug_bride_and_groom) }}" class="btn btn-info btn-sm">Report</a>
                                <a href="{{ route('edit', $item->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                <form action="{{ route('destroy', $item->id) }}" style="display: inline;">
                                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
