@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="groom">Groom</label>
                            <input type="text" class="form-control" name="groom" placeholder="Jason" required>
                        </div>
                        <div class="form-group">
                            <label for="full_name_groom">Full Name Groom</label>
                            <input type="text" class="form-control" name="full_name_groom" placeholder="Jason William Winata" required>
                        </div>
                        <div class="form-group">
                            <label for="bride">Bride</label>
                            <input type="text" class="form-control" name="bride" placeholder="Renata" required>
                        </div>
                        <div class="form-group">
                            <label for="full_name_bride">Full Name Bride</label>
                            <input type="text" class="form-control" name="full_name_bride" placeholder="Renata Moeloek" required>
                        </div>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" class="form-control" name="date" required>
                        </div>
                        <div class="form-group">
                            <label for="event_house">Event House</label>
                            <select name="event_house" id="event_house" class="form-control">
                                <option value="Wanita">Bride</option>
                                <option value="Pria">Groom</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" placeholder="Ruko RMI Blok I No.7" required>
                        </div>
                        <div class="form-group">
                            <label for="district">District</label>
                            <input type="text" class="form-control" name="district" placeholder="Gubeng" required>
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" name="city" placeholder="Surabaya" required>
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <input type="text" class="form-control" name="province" placeholder="Jawa Timur" required>
                        </div>
                        <div class="form-group">
                            <label for="the_son_of_the_groom">The son of the groom</label>
                            <input type="text" class="form-control" name="the_son_of_the_groom" placeholder="Putra Pertama" required>
                        </div>
                        <div class="form-group">
                            <label for="the_father_of_the_groom">The father of the groom</label>
                            <input type="text" class="form-control" name="the_father_of_the_groom" required>
                        </div>
                        <div class="form-group">
                            <label for="the_mother_of_the_groom">The mother of the groom</label>
                            <input type="text" class="form-control" name="the_mother_of_the_groom" required>
                        </div>
                        <div class="form-group">
                            <label for="the_daughter_of_the_bride">The daughter of the bride</label>
                            <input type="text" class="form-control" name="the_daughter_of_the_bride" placeholder="Putri Pertama" required>
                        </div>
                        <div class="form-group">
                            <label for="the_father_of_the_bride">The father of the bride</label>
                            <input type="text" class="form-control" name="the_father_of_the_bride" required>
                        </div>
                        <div class="form-group">
                            <label for="the_mother_of_the_bride">The mother of the bride</label>
                            <input type="text" class="form-control" name="the_mother_of_the_bride" required>
                        </div>
                        <div class="form-group">
                            <label for="name_of_the_day">Name of the day</label>
                            <input type="text" class="form-control" name="name_of_the_day" placeholder="Sabtu" required>
                        </div>
                        <div class="form-group">
                            <label for="name_of_the_month">Name of the month</label>
                            <input type="text" class="form-control" name="name_of_the_month" placeholder="November" required>
                        </div>
                        <div class="form-group">
                            <label for="year">Year</label>
                            <input type="text" class="form-control" name="year" placeholder="2021" required>
                        </div>
                        <div class="form-group">
                            <label for="time">Time</label>
                            <input type="text" class="form-control" name="time" placeholder="16.00 WIB - Selesai" required>
                        </div>
                        <div class="form-group">
                            <label for="count_down">Time and day count down</label>
                            <input type="datetime-local" class="form-control" name="count_down" required>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Latitude</label>
                            <input type="latitude" class="form-control" name="latitude" placeholder="0.5249867887879" required>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Longitude</label>
                            <input type="longitude" class="form-control" name="longitude" placeholder="101.42698287963" required>
                        </div>
                        <div class="form-group">
                            <label for="template">Template</label>
                            <select name="template" id="template" class="form-control">
                                <option value="1">Template One</option>
                                <option value="2">Template Two</option>
                                <option value="3">Template Three</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
