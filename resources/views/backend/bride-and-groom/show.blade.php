@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detail</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="groom">Groom</label>
                        <input type="text" class="form-control" name="groom" readonly value="{{ $data->groom }}">
                    </div>
                    <div class="form-group">
                        <label for="full_name_groom">Full Name Groom</label>
                        <input type="text" class="form-control" name="full_name_groom" readonly value="{{ $data->full_name_groom }}">
                    </div>
                    <div class="form-group">
                        <label for="bride">Bride</label>
                        <input type="text" class="form-control" name="bride" readonly value="{{ $data->bride }}">
                    </div>
                    <div class="form-group">
                        <label for="full_name_bride">Full Name Bride</label>
                        <input type="text" class="form-control" name="full_name_bride" readonly value="{{ $data->full_name_bride }}">
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" name="date" readonly value="{{ $data->date }}">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" readonly value="{{ $data->address }}">
                    </div>
                    <div class="form-group">
                        <label for="district">District</label>
                        <input type="text" class="form-control" name="district" readonly value="{{ $data->district }}">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" name="city" readonly value="{{ $data->city }}">
                    </div>
                    <div class="form-group">
                        <label for="province">Province</label>
                        <input type="text" class="form-control" name="province" readonly value="{{ $data->province }}">
                    </div>
                    <div class="form-group">
                        <label for="event_house">Event House</label>
                        <input type="text" class="form-control" name="event_house" readonly value="{{ $data->event_house }}">
                    </div>
                    <div class="form-group">
                        <label for="the_son_of_the_groom">The son of the groom</label>
                        <input type="text" class="form-control" name="the_son_of_the_groom" readonly value="{{ $data->the_son_of_the_groom }}">
                    </div>
                    <div class="form-group">
                        <label for="the_father_of_the_groom">The father of the groom</label>
                        <input type="text" class="form-control" name="the_father_of_the_groom" readonly value="{{ $data->the_father_of_the_groom }}">
                    </div>
                    <div class="form-group">
                        <label for="the_mother_of_the_groom">The mother of the groom</label>
                        <input type="text" class="form-control" name="the_mother_of_the_groom" readonly value="{{ $data->the_mother_of_the_groom }}">
                    </div>
                    <div class="form-group">
                        <label for="the_daughter_of_the_bride">The daughter of the bride</label>
                        <input type="text" class="form-control" name="the_daughter_of_the_bride" readonly value="{{ $data->the_daughter_of_the_bride }}">
                    </div>
                    <div class="form-group">
                        <label for="the_father_of_the_bride">The father of the bride</label>
                        <input type="text" class="form-control" name="the_father_of_the_bride" readonly value="{{ $data->the_father_of_the_bride }}">
                    </div>
                    <div class="form-group">
                        <label for="the_mother_of_the_bride">The mother of the bride</label>
                        <input type="text" class="form-control" name="the_mother_of_the_bride" readonly value="{{ $data->the_mother_of_the_bride }}">
                    </div>
                    <div class="form-group">
                        <label for="name_of_the_day">Name of the day</label>
                        <input type="text" class="form-control" name="name_of_the_day" readonly value="{{ $data->name_of_the_day }}">
                    </div>
                    <div class="form-group">
                        <label for="name_of_the_month">Name of the month</label>
                        <input type="text" class="form-control" name="name_of_the_month" readonly value="{{ $data->name_of_the_month }}">
                    </div>
                    <div class="form-group">
                        <label for="year">Year</label>
                        <input type="text" class="form-control" name="year" readonly value="{{ $data->year }}">
                    </div>
                    <div class="form-group">
                        <label for="time">Time</label>
                        <input type="text" class="form-control" name="time" readonly value="{{ $data->time }}">
                    </div>
                    <div class="form-group">
                        <label for="count_down">Time and day count down</label>
                        <input type="datetime-local" class="form-control" name="count_down" readonly value="{{ date('Y-m-d\TH:i', strtotime($data->count_down))  }}">
                    </div>
                    <div class="form-group">
                        <label for="latitude">Latitude</label>
                        <input type="text" class="form-control" name="latitude" readonly value="{{ $data->lat }}">
                    </div>
                    <div class="form-group">
                        <label for="longitude">Longitude</label>
                        <input type="text" class="form-control" name="longitude" readonly value="{{ $data->long }}">
                    </div>
                    <div class="form-group">
                        <label for="template">Template</label>
                        <select name="template" id="template" readonly class="form-control">
                            <option value="1" @if($data->template == 1) selected @endif>Template One</option>
                            <option value="2" @if($data->template == 2) selected @endif>Template Two</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <form action="{{ route('exportExcel', [$data->id, $data->slug_bride_and_groom]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="excel">Import Excel</label>
                        <input type="file" class="form-control" name="excel" required="">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{ route('index') }}" class="btn btn-danger">Batal</a>
                    </form>
                </div>

                <hr>

                @if (count($data->guest) > 0)
                <div class="col-md-12 table-responsive">
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah">
                        Create Data Guest 
                    </a>
                    <table class="table" id="example">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Guest Name</td>
                                <td>Guest Description</td>
                                <td>Link</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data->guest as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    <a href="{{ config('app.url_link').$item->slug_guest }}" target="_blank">
                                        {{ config('app.url_link').$item->slug_guest }}
                                    </a>
                                </td>
                                <td>
                                    <button class="btn btn-warning btn-sm" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="insertFormEdit(this);" data-item="{{$item}}">Edit</button>
                                    <form action="{{ route('destroy.guest', $item->id) }}" style="display: inline;">
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4><i class="fa fa-plus"></i> Tambah data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
              <div class="modal-body">
                <form action="{{ route('store.guest', [$data->id, $data->slug_bride_and_groom]) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Guest Name</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description">Description Guest</label>
                        <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure?')"> Simpan</button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> Batal</button>
                    </div> 
                </form>
              </div>
        </div>s
    </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4><i class="fa fa-edit"></i> Update Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
              <div class="modal-body">
                <form role="form" method="POST" id="formEdit" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Guest Name</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description">Guest Description</label>
                        <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Anda yakin ingin menyelesaikan?')"> Update</button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> Batal</button>
                    </div>  
              </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" defer> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer> </script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" defer> </script>

<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]
        } );
    } );

    function insertFormEdit(button){
        var item = $(button).data('item');
        var nameParamSlug = "{{ request()->segment(3) }}";
        $('form#formEdit').attr('action',`{{ url("alpha/update-guest") }}/${item.id}/${nameParamSlug}`);
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #description').val(item.description);
    }
</script>

@endsection
