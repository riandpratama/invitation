<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keyword" content="alphacreativee, alpha, creativee, invitation, undangan web, undangan video, undangan gambar">
        <link rel="shortcut icon" href="{{ asset('assets/images/web.png') }}">
        <link rel="stylesheet" href="https://fishtoyo.github.io/website/codepen/css/default.css">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrapv4.3.1.css') }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <title>Web Invitation - Alphacreative</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
        <iframe class="iframe-preview iframe-preview-mobile" width="100%" src="{{ config('app.url').request()->segment(1) }}">
        </iframe>
    </body>
</html>