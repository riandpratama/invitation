
    
    

  <!-- Histats.com  START  (aync)-->
{{-- <script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,4354987,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4354987&101" alt="" border="0"></a></noscript> --}}
<!-- Histats.com  END  -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>UNDANGAN PERNIKAHAN SANTI & WAHYU</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta name="description" content="Undangan Website Pernikahan">
        <meta property="og:title" content="UNDANGAN PERNIKAHAN SANTI & WAHYU" />
        <meta property="og:url" content="https://simanten.com" />
        <meta property="og:image" content="https://inv.simanten.com/images/thumbnail/2631-262.jpg">
        <meta property="og:description" content="15  November  2020 ">
    	<meta property="og:image:type" content="image/jpeg" />

        <!-- CSS -->
        <link href="{{ asset('assets/css/royalslider.css') }}" rel="stylesheet" class="rs-file">
        <link href="{{ asset('assets/css/clear.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/jquery-ui-1.8.22.custom.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/vasily-polovnyov.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/rs-minimal-white.css') }}" rel="stylesheet" class="rs-file" >
        <link href="{{ asset('assets/css/swiper.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/swiper.min2.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">      
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/slider.css') }}" rel="stylesheet">

        <!-- slider JS files -->
        <script src="{{ asset('assets/js/jquery-1.8.3.min.js') }}" class="rs-file"></script>
        <script src="{{ asset('assets/js/jquery.royalslider.min.js') }}" class="rs-file"></script>
        <script src="{{ asset('assets/js/jquery.easing-1.3.js') }}" class="rs-file"></script>
       
        <!-- syntax highlighter -->
        <script src="{{ asset('assets/js/highlight.pack.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-ui-1.8.22.custom.min.js') }}"></script>
        
        <!-- preview-related stylesheets -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Redressed&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Playball&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Cinzel&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playball&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Lobster+Two&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Mr+De+Haviland&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@500&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div id="box" class="box">
        <div style="position:absolute;top:0px;bottom:0px;right:0px;left:0px;background: rgba( 0,0,0,0)"></div>
            <div class="cover">                    
            <img src="https://inv.simanten.com/images/library/frame_penerima01.png" style="width: 170px;margin-left: -85px;margin-top: -160px;">
            <div style="color: #c79931;width: 300px;margin-left: -150px;text-align: center;margin-top: -120px;font-family: 'Cormorant Garamond', serif;font-size: 52px">
                <table style="width: 100%;color: #977655;">
                    <tr>
                        <td style="width: 100px;text-align: right">D</td>
                        <td style="width: 1px;text-align: center">|</td>
                        <td style="width: 100px;text-align: left">R</td>
                    </tr>
                </table>        
            </div>
            <div style="width: 300px;margin-left: -150px;text-align: center;margin-top: 60px">
                <p style="color:#977655;font-size: 12px;">Kepada Yth :</p>
                <p style="color:#977655;font-size: 17px;margin-bottom: 10px">Someone</p>
                <img src="https://inv.simanten.com/images/library/line6.png" style="width: 170px;margin-top:-7px;margin-bottom:3px;">
                <div style="color: #977655">( 2021 )</div>
            </div>
            
           <div>
                <div class="button" 
                    style="padding:15px 0px;height:20px;width: 150px;margin-left: -75px;text-align: center;margin-top: 40px;font-size: 17px;color:#977655">
                    <small><b>Buka Undangan</b></small>
                </div>
            </div>
        </div>
        </div>

        <button id="close" class="buttonx" 
                style="border: 0px solid #333;position: absolute;top: 0px ;bottom: 0px;left: 0px;right: 0px;width: 100%;background: rgba(0,0,0,0); z-index: 1333" 
                onclick="Close()">
        </button>

        <div  class="page wrapper">  
            <!-- slider code start -->
            <div class="row clearfix">
                <div class="col span_4 fwImage">
                    <div id="slider-with-blocks-1" class="royalSlider rsMinW">
                        <div class="rsContent slide1 box">
                            <div style="position:absolute;top:0px;bottom:0px;right:0px;left:0px;background: rgba( 0,0,0,0)"></div>
                            <div class="cover">                    
                                <img src="https://inv.simanten.com/images/library/frame_penerima01.png" style="width: 170px;margin-left: -85px;margin-top: -160px;">
                                <div style="color: #977655;width: 300px;margin-left: -150px;text-align: center;margin-top: -120px;font-family: 'Cormorant Garamond', serif;font-size: 52px">
                                    <table style="width: 100%;color: #977655;">
                                        <tr>
                                            <td style="width: 100px;text-align: right">D</td>
                                            <td style="width: 1px;text-align: center">|</td>
                                            <td style="width: 100px;text-align: left">R</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 300px;margin-left: -150px;text-align: center;margin-top: 64px">
                                    <p style="color:#977655;font-size: 12px;">Kepada Yth :</p>
                                    <p style="color:#977655;font-size: 17px;margin-bottom: 10px;margin-top: 16px">Someone</p>
                                    <img src="https://inv.simanten.com/images/library/line6.png" style="width: 170px;margin-top:-1px;margin-bottom:5px;">
                                    <div style="color: #977655;font-size: 13px">( 2021 )</div>
                                </div>
                                <div class="rsTmb" data-rsw="632" data-rsh="500">
                                    <i class="fa fa-user" style="margin-top: 10px;font-size: 17px"></i>
                                    <div style="font-size: 10px">Penerima</div>
                                </div>
                            </div>
                        </div>
                        <div class="rsContent slide1"  style="background-color: #fafafa">
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png" 
                                style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l2.png" 
                                style="width:90px;bottom:0px;position: absolute;left: 0px;z-index: 0;">  
                            <div class="cover">
                                <div class="cover_nama" style="width: 330px;color: #fff;margin-left: -165px;margin-top: -200px;text-align:center">
                                    <div style="margin-bottom:17px" class="rsABlock txtCent" data-move-offset="50" data-delay="100" data-speed="800" data-move-effect="bottom"><img src="https://inv.simanten.com/images/tema/flowerbasic04/cover.png" style="width: 290px;"></div>
                                </div>
                            </div>    
                            <div class="cover" style="margin-left: -150px;margin-top: -138px;font-family: 'Josefin Slab', serif;text-align:center">
                                <div style="width: 300px;">
                                    <div style="color: #666;font-family: 'Redressed', serif;font-size: 22px;" class="rsABlock" 
                                        data-move-offset="50" data-delay="100" data-speed="1000" data-move-effect="none">
                                        The Wedding
                                    </div>
                                    <div class="cover_nama rsABlock txtCent" 
                                        style="margin-top: 10px;font-family: 'Playball', cursive;color: #d0b292" data-move-offset="50" data-delay="100" data-speed="1000" data-move-effect="none">
                                                        
                                        <div style="color: #999;font-family: 'Dancing Script', cursive;margin-top: 45px;font-size: 20px;">&</div>
                                        <div style="margin-top: -45px;font-size: 27px">
                                            <div class='rsABlock' data-move-offset='50' 
                                                data-delay='100' data-speed='1000' data-move-effect='right'>
                                                Dewi
                                            </div>
                                            <br><br><br>
                                            <div class='rsABlock' data-move-offset='50' data-delay='100' data-speed='1000' data-move-effect='left'>
                                                Riand
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div style="font-family: 'Lato', serif;font-size: 14px;margin-top: 140px;color:#666 " 
                                            class="rsABlock txtCent" data-move-offset="50" data-delay="100" data-speed="800" data-move-effect="top">
                                            15 . 11 . 2021 
                                            <br>
                                            <small>
                                                <b>Rumah Mempelai Wanita</b><br>
                                                Jl. Ibrahim <br>
                                                Kec. Plumpang, Tuban
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="position: absolute;bottom: 50px;left: 0px ; right: 0px;text-align: center" 
                                class="rsABlock txtCent" data-move-offset="50" data-delay="2333" data-speed="800" data-move-effect="bottom">
                                <div style="text-align: center;color: #bd9f5b" >
                                    <div class="animation_swipup">
                                        <div>                    
                                            <i class="fa fa-chevron-up" style="color: #bd9f5b;font-size: 15px;margin-left:0px;"></i>
                                        </div>
                                        <div style="margin-top: -5px">                    
                                            <i class="fa fa-chevron-up" style="color: #bd9f5b;font-size: 15px;margin-left:0px;"></i>
                                        </div>
                                    </div>
                                    <div style="font-size: 10px;margin-top:-20px">
                                        Geser ke Atas
                                    </div>
                                </div>
                            </div>

                            <div class="rsTmb" data-rsw="632" data-rsh="500">
                                <i class="fa fa-address-book" style="margin-top: 10px;font-size: 17px"></i>
                                <div style="font-size: 10px">Sampul</div>
                            </div>
                        </div>

                        <div class="rsContent slide2 bg-content">
                            <div class="rsABlock" data-move-offset="450" data-delay="0" data-speed="1200" data-move-effect="none" 
                                style="background-color: rgb(255, 255, 255, 0.9);border-radius: 10px;position: absolute;top: 20px;bottom: 20px;left: 20px;right: 20px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.2), 0 1px 8px 0 rgba(50, 50, 50, 0.7);z-index: 0;">
                            </div>
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"
                                src="https://inv.simanten.com/images/tema/flowerbasic04/top_r.png" 
                                style="width:140px;top:0px;position: absolute;right: 0px;z-index: 0;"> 
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                src="https://inv.simanten.com/images/tema/flowerbasic04/top_l.png" 
                                style="width:90px;top:0px;position: absolute;left: 0px;z-index: 0;">                                    
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png" 
                                style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                            <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l.png" 
                                style="width:140px;bottom:0px;position: absolute;left: 0px;z-index: 0;">

                            <div class="cover" style="text-align:center">
                                <div style='margin-top:40px'></div>
                                <div style="margin-left:-150px;width: 300px;margin-top: -230px;">
                                    <div style="font-size:12px;margin-bottom: 20px" class="rsABlock" data-move-offset="50" data-delay="100" 
                                        data-speed="1000" data-move-effect="top">
                                        <div>
                                            <img src="https://inv.simanten.com/images/librari/bismillah.png" style="width: 140px"/>
                                        </div>
                                        <br>
                                        <div>
                                            <i>Assalamu'alaikum warahmatullahi wabarakatuh</i>
                                        </div>
                                        <div>
                                            Dengan memohon rahmat dan ridho Allah SWT,<br>
                                            Mohon do'a restu Bapak/Ibu/Saudara dalam rangka melangsungkan pernikahan putra putri kami :
                                        </div>
                                    </div>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan='2'>
                                                <div></div>
                                                </td></tr>
                                                <tr>
                                                    <td style="text-align: center;vertical-align: top" colspan="2">
                                                        <div class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="bottom">
                                                            <span class="font_nama_mempelai">Dewi Munadhirotul Alfi</span><br>
                                                            <img src="https://inv.simanten.com/images/library/line1.png" style="width: 180px;"> 
                                                            <div class="font_lebel_mempelai">Putri Pertama</div>
                                                            <span class="font_ortu">
                                                                Alm. Bapak Mohammad Hanafi <br>Ibu Mutiatil Khorimah
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="padding: 5px;text-align:center;">
                                                        <span class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="none" 
                                                                style="font-family: 'Dancing Script', cursive;font-size: 25px;">
                                                        &
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;vertical-align: top" colspan="2">
                                                        <div class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="bottom">
                                                            <span class="font_nama_mempelai">Riand Pratama Putra</span><br>
                                                            <img src="https://inv.simanten.com/images/library/line1.png" style="width: 180px;"> 
                                                            <div class="font_lebel_mempelai">Putra Pertama</div>
                                                            <span class="font_ortu">
                                                                Bapak Slamet Riyadi<br>
                                                                Ibu Siti Fatmawati
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="rsTmb" data-rsw="632" data-rsh="500">
                                        <i class="fa fa-heart" style="margin-top: 10px;font-size: 17px"></i>
                                        <div style="font-size: 10px">Mempelai</div>
                                    </div>
                                </div>
                                <div class="rsContent slide2 bg-content">
                                    <div class="rsABlock" data-move-offset="450" data-delay="0" data-speed="1200" data-move-effect="none" 
                                        style="background-color: rgb(255, 255, 255, 0.9);border-radius: 10px;position: absolute;top: 20px;bottom: 20px;left: 20px;right: 20px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.2), 0 1px 8px 0 rgba(50, 50, 50, 0.7);z-index: 0;">
                                    </div>
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_r.png" 
                                        style="width:140px;top:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_l.png" 
                                        style="width:90px;top:0px;position: absolute;left: 0px;z-index: 0;">                                    
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png" 
                                        style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none" 
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l.png" 
                                        style="width:140px;bottom:0px;position: absolute;left: 0px;z-index: 0;">                                    
                                    <div class="cover">
                                        <div style="margin-left:-175px;width: 350px;margin-top: -230px;text-align:center">
                                            <div style="width: 350px;">
                                                <div style="font-size: 24px;font-family: 'Redressed', cursive;"class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                    Rangkaian Acara
                                                </div>
                
                                                <div style="font-size: 14px;"> 

                                                    <div style="padding: 15px 50px 5px 50px;font-size: 24px;font-family: 'Dancing Script', cursive;" class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <img src="https://inv.simanten.com/images/library/line_b19973.jpg" style="height:1px;width: 100%;padding:0px 10px 5px 0px;">
                                                                </td>
                                                                <td style="color: #b19973;text-align: center;padding: 0px 15px;white-space: nowrap;width: 1px">
                                                                    Resepsi
                                                                </td>
                                                                <td>
                                                                    <img src="https://inv.simanten.com/images/library/line_b19973.jpg" style="height:1px;width: 100%;padding:0px 10px 5px 0px;">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <table style="width: 100%;font-family: 'Josefin Slab', serif;">
                                                        <tr>
                                                            <td></td>
                                                            <td style="text-align: right;font-size: 18px;padding-right:10px;width: 120px;vertical-align: middle;white-space: nowrap;">
                                                                <div class="rsABlock" data-move-offset="50" data-delay="600" data-speed="800" data-move-effect="right">
                                                                    <b>
                                                                        Minggu
                                                                    </b>
                                                                    <div style="margin-top:3px;">
                                                                        <small>
                                                                            16:00 WIB - Selesai
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="width: 64px;text-align: center;vertical-align: middle">
                                                                <div class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top" style="border-radius: 58px;width:58px;height:58px;border: 3px solid #fff; font-size: 36px;color: #fff;background-color: #333;box-shadow: 0 1px 2px 0 rgba(150, 150, 150, 0.5), 0 1px 8px 0 rgba(150, 150, 150, 0.7)">
                                                                    <div style="margin-top:13px;margin-left:-1px">
                                                                        <b>15</b>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="text-align: left;font-size: 18px;padding-left:10px;width: 120px;vertical-align: middle;white-space: nowrap;">
                                                                <div class="rsABlock" data-move-offset="50" data-delay="600" data-speed="800" data-move-effect="left">
                                                                    <b>November </b>
                                                                    <div style="margin-top:3px;">
                                                                        <small>2021</small>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-top: 8px;font-size: 12px;" class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                        <b>Rumah Mempelai Wanita</b><br>Jl. Ibrahim<br>Plumpang, Tuban
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding: 15px 30px 15px 30px;position: relative">
                                                <table style="width: 100%;text-align:center">
                                                    <tr>
                                                        <td colspan="9"><div style="font-size: 13px">
                                                                <div class="rsABlock" data-move-offset="50" data-delay="1000" data-speed="1000" data-move-effect="left">
                                                                    Hitung Mundur Acara <b style="color:#b19973">Resepsi</b></div><br>        
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td style="width: 20%;text-align: center;">
                                                            <div class="rsABlock" data-move-offset="50" data-delay="1200" data-speed="800" data-move-effect="bottom" style="box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, .5), inset 0px 4px 1px 1px white, inset 0px -3px 1px 1px rgba(183,180,180,.5);background-color: #fff;border-radius: 5px;color:#333;padding: 7px 0px;width: 60px;font-size: 15px">
                                                                <b><span id="hari"></span></b>
                                                                <div>
                                                                    <small>Hari</small>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td style="width: 20%;text-align: center">
                                                            <div class="rsABlock" data-move-offset="50" data-delay="1200" data-speed="800" data-move-effect="bottom" style="box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, .5), inset 0px 4px 1px 1px white, inset 0px -3px 1px 1px rgba(183,180,180,.5);background-color: #fff;border-radius: 5px;color:#333;padding: 7px 0px;width: 60px;font-size: 15px">
                                                                <b><span id="jam"></span></b>
                                                                <div>
                                                                    <small>Jam</small>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td style="width: 20%;text-align: center">
                                                            <div class="rsABlock" data-move-offset="50" data-delay="1200" data-speed="800" data-move-effect="bottom" style="box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, .5), inset 0px 4px 1px 1px white, inset 0px -3px 1px 1px rgba(183,180,180,.5);background-color: #fff;border-radius: 5px;color:#333;padding: 7px 0px;width: 60px;font-size: 15px">
                                                                <b><span id="menit"></span></b>
                                                                <div>
                                                                    <small>Menit</small>
                                                                </div>
                                                            </div>                                            
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td style="width: 20%;text-align: center">
                                                            <div class="rsABlock" data-move-offset="50" data-delay="1200" data-speed="800" data-move-effect="bottom" style="box-shadow: 0px 3px 10px 0px rgba(0, 0, 0, .5), inset 0px 4px 1px 1px white, inset 0px -3px 1px 1px rgba(183,180,180,.5);background-color: #fff;border-radius: 5px;color:#333;padding: 7px 0px;width: 60px;font-size: 15px">
                                                                <b><span id="detik"></span></b>
                                                                <div>
                                                                    <small>Detik</small>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rsTmb" data-rsw="632" data-rsh="500">
                                        <i class="fa fa-calendar-check-o" style="margin-top: 10px;font-size: 17px"></i>
                                        <div style="font-size: 10px">Acara</div>
                                    </div>
                                </div>
    
                                <div class="rsContent slide2 bg-content">
                                    <div class="rsABlock" data-move-offset="450" data-delay="0" data-speed="1200" data-move-effect="none" 
                                        style="background-color: rgb(255, 255, 255, 0.9);border-radius: 10px;position: absolute;top: 20px;bottom: 20px;left: 20px;right: 20px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.2), 0 1px 8px 0 rgba(50, 50, 50, 0.7);z-index: 0;">
                                    </div>
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_r.png"
                                        style="width:140px;top:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_l.png"
                                        style="width:90px;top:0px;position: absolute;left: 0px;z-index: 0;">                                    
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png"
                                        style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l.png"
                                        style="width:140px;bottom:0px;position: absolute;left: 0px;z-index: 0;">                                    

                                    <div class="cover">
                                        <div style="margin-left:-175px;width: 350px;margin-top: -230px;text-align:center">
                                            <div style="width: 350px;">
                                                <div style="font-size: 24px;font-family: 'Redressed', cursive;"class="rsABlock" 
                                                    data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                    Peta Lokasi
                                                </div>
                                                <div style="margin-top: 15px" class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                    <img src="https://inv.simanten.com/images/library/line_b19973.jpg" style="height:1px;width: 70px;padding:0px 10px 10px 10px;">
                                                    <div style="font-size: 12px;margin-bottom: 10px">
                                                        <b>Rumah Mempelai Wanita</b><br>
                                                        Jl. Kedung Baruk Gg. XVIII<br>
                                                        Kedung Baruk, Kec. Rungkut, Surabaya
                                                    </div>
                                                    <div id="gmap_markers" class="gllpMap"></div>
                                                    <div style="text-align: center;margin-top:10px;z-index:1000">
                                                        <a href='http://www.google.com/maps/place/-7.316107,112.775180' target="_blank">
                                                            <button type="button" class="button" style="width:270px">
                                                                <img src="https://inv.simanten.com/images/librari/gmap.png" style="width: 30px;"> 
                                                                <span style="position: relative;bottom: 8px">
                                                                    <b>Buka di GMAPS</b>
                                                                </span>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rsTmb" data-rsw="632" data-rsh="500">
                                        <i class="fa fa-map-o" style="margin-top: 10px;font-size: 17px"></i>
                                        <div style="font-size: 10px">Peta</div>
                                    </div>
                                </div>

                                <div class="rsContent slide2 bg-content">
                                    <div class="rsABlock" data-move-offset="450" data-delay="0" data-speed="1200" data-move-effect="none" 
                                        style="background-color: rgb(255, 255, 255, 0.9);border-radius: 10px;position: absolute;top: 20px;bottom: 20px;left: 20px;right: 20px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.2), 0 1px 8px 0 rgba(50, 50, 50, 0.7);z-index: 0;">
                                    </div>
                                        <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                            src="https://inv.simanten.com/images/tema/flowerbasic04/top_r.png" style="width:140px;top:0px;position: absolute;right: 0px;z-index: 0;"> 
                                        <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                            src="https://inv.simanten.com/images/tema/flowerbasic04/top_l.png" style="width:90px;top:0px;position: absolute;left: 0px;z-index: 0;">               
                                        <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                            src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png" style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                                        <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                            src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l.png" style="width:140px;bottom:0px;position: absolute;left: 0px;z-index: 0;">                                    
                                        <div class="cover">
                                            <div style="margin-left:-175px;width: 350px;margin-top: -220px;text-align:center">
                                                <div style="width: 350px;">
                                                    <div style="font-size: 24px;font-family: 'Redressed', cursive;"class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                        Pencegahan Covid-19
                                                    </div>

                                                    <div style="font-size: 12px;margin-top: 15px" class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">
                                                        <img src="https://inv.simanten.com/images/library/line_b19973.jpg" style="height:1px;width: 70px;padding:0px 10px 10px 10px;">
                                                        <div style="margin-bottom: 10px">
                                                            Acara ini akan diselenggarakan dengan mematuhi<br>
                                                            protokol pencegahan penyebaran COVID-19.
                                                        </div>
                                                        <div style="padding:5px 25px">
                                                            <table style="width:100%"> 
                                                                <tr>
                                                                    <td style="width:50px">
                                                                        <img src="https://inv.simanten.com/images/librari/covid/masker_black.png" style="height:auto;width: 50px;">
                                                                    </td>
                                                                    <td style="vertical-align:top;padding:5px 10px;text-align:left">
                                                                        Tamu undangan menggunakan masker.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td style="vertical-align:top;padding:5px 10px;text-align:right">
                                                                        Suhu tubuh normal<br>
                                                                        (dibawah 37,5°C)
                                                                    </td>
                                                                    <td style="width:50px">
                                                                        <img src="https://inv.simanten.com/images/librari/covid/temperatur_black.png" style="height:auto;width: 50px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td style="width:50px">
                                                                        <img src="https://inv.simanten.com/images/librari/covid/jaga_jarak_black.png" style="height:auto;width: 50px;">
                                                                    </td>
                                                                    <td style="vertical-align:top;padding:5px 10px;text-align:left">
                                                                        Jaga jarak antar orang sekitar<br>
                                                                        2 meter.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td style="vertical-align:top;padding:5px 10px;text-align:right">
                                                                    Cuci tangan menggunakan air dan sabun atau menggunakan hand sanitizer.
                                                                    </td>
                                                                    <td style="width:50px">
                                                                        <img src="https://inv.simanten.com/images/librari/covid/cuci_tangan_black.png" style="height:auto;width: 50px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <br>
                                                        <div>
                                                            Bagi para tamu undangan diharapkan mengikuti<br>
                                                            protokol pencegahan COVID-19.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="rsTmb" data-rsw="632" data-rsh="500">
                                        <i class="fa fa-book" style="margin-top: 10px;font-size: 17px"></i>
                                        <div style="font-size: 10px">
                                            Protokol
                                        </div>
                                    </div>
                                </div>

                                <div class="rsContent slide2 bg-content">
                                    <div class="rsABlock" data-move-offset="450" data-delay="0" data-speed="1200" data-move-effect="none" 
                                        style="background-color: rgb(255, 255, 255, 0.9);border-radius: 10px;position: absolute;top: 20px;bottom: 20px;left: 20px;right: 20px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.2), 0 1px 8px 0 rgba(50, 50, 50, 0.7);z-index: 0;">
                                    </div>
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_r.png" 
                                        style="width:140px;top:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/top_l.png" 
                                        style="width:90px;top:0px;position: absolute;left: 0px;z-index: 0;">                                    
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_r.png" 
                                        style="width:90px;bottom:0px;position: absolute;right: 0px;z-index: 0;"> 
                                    <img class="rsABlock" data-move-offset="450" data-delay="600" data-speed="800" data-move-effect="none"  
                                        src="https://inv.simanten.com/images/tema/flowerbasic04/bottom_l.png" 
                                        style="width:140px;bottom:0px;position: absolute;left: 0px;z-index: 0;">                                    

                                        <div class="cover">
                                            <div style="margin-left:-175px;width: 350px;margin-top: -240px;text-align:center">
                                                <div style="width: 350px;">
                                                    <div style="margin-top: 15px" class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="top">

                                                        <div style="font-size: 12px;margin-bottom: 15px;padding: 0px 20px">
                                                            Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila bapak/ibu/saudara/i berkenan hadir untuk memberikan
                                                            doa restu kepada kedua mempelai.<br><br>
                                                            "Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu isteri-isteri dari jenismu sendiri, 
                                                            supaya kamu merasa tenang dan tentram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. 
                                                            Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir."
                                                            <br>(QS. Ar-Rum: 21)<br>
                                                            <i>Wassalamu'alaikum Warahmatullahi Wabarakatuh</i></span><br><br><div>Turut berbahagia<br>Segenap keluarga besar</div></div>
                                                            
                                                            <div style="padding: 0px 20px;color:#b19973;"class="rsABlock" data-move-offset="50" data-delay="200" 
                                                                data-speed="800" data-move-effect="top">
                                                                <div style="font-size: 27px;font-family: 'Playball', cursive;">
                                                                    Santi
                                                                    <span style='color:#333'>&</span>  
                                                                    Wahyu
                                                                </div>
                                                            </div>
                                                            <div style="text-align: center;margin-top: 70px" id="btn_ucapan">
                                                                <button type="button" class="button" onclick="Pesan()" style="margin-top: -42px;">
                                                                    <img src="https://inv.simanten.com/images/librari/pen_black.png" style="width: 30px;" /> 
                                                                    <span style="position: relative;bottom: 8px">
                                                                        <b>Tulis Ucapan & Doa</b>
                                                                    </span>
                                                                </button>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rsABlock" data-move-offset="50" data-delay="200" data-speed="800" data-move-effect="bottom" 
                                                style="position: absolute;bottom: 60px;text-align: center;right: 0px;left: 0px">
                                                <div id="berhsil" 
                                                    style="display:none;background-color:#555;color:#fff;margin:10px 30px;padding :10px 0px;font-size:12px;box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.5), 0 1px 8px 0 rgba(50, 50, 50, 0.7);">
                                                    Terimakasih,<br>atas ucapan yang Anda berikan.
                                                </div>
                                                    <table style="width: 100%;text-align:center;color:#333;" class="partner">
                                                        <tr>
                                                            <td colspan="5">
                                                                <span style="font-size: 11px">
                                                                    <div style="margin-bottom:10px;">
                                                                        Website Invitation Supported By <b class="partner_by">Simanten</b>
                                                                    </span>
                                                                </div>    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:8%"></td>
                                                            <td style="width: 28%"><img src="https://inv.simanten.com/images/librari/wa.png" style="width: 30px"></td>
                                                            <td style="width: 28%"><img src="https://inv.simanten.com/images/librari/ig.png" style="width: 30px"></td>
                                                            <td style="width: 28%"><img src="https://inv.simanten.com/images/librari/web.png" style="width: 30px"></td>
                                                            <td style="width:8%"></td>
                                                        </tr>
                                                        <tr>
                                                            <th></th>
                                                            <th style="font-size: 11px;vertical-align: top">+6288232526454</th>
                                                            <th style="font-size: 11px;vertical-align: top">simanten</th>
                                                            <th style="font-size: 11px;vertical-align: top">simanten.com</th>
                                                            <th></th>
                                                        </tr>
                                                    </table>

                                                </div>
                                                <div class="rsTmb" data-rsw="632" data-rsh="500">
                                                    <i class="fa fa-certificate" style="margin-top: 10px;font-size: 17px"></i>
                                                    <div style="font-size: 10px">Penutup</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="" id="pesan" style="position: absolute;bottom: 0px;top: 0px;left: 0px;right: 0px;background-color: #ccbfb9;display: none;z-index: 1333">
                                    <div style="padding: 0px;position: relative;background-color: #ccbfb9;">
                                        <div style="padding: 20px;position: relative;">
                                            <div style="border-radius: 15px;background-color: #dadfe5;padding: 0px 15px 10px 15px;border: 5px solid #fff;box-shadow: 0 2px 7px rgba(0, 0, 0, 0.7);   ">
                                                <div style="text-align: center;color: #39404a;margin-bottom: 10px;margin-top:20px">
                                                    <h4><img src="https://inv.simanten.com/images/librari/pen_grey.png" style="height: 35px;margin-bottom: 0px;">
                                                        <div>UCAPAN & DOA</div>
                                                    </h4>
                                                </div>
                                                <hr>
                                                <table style="width: 100%;color: #39404a">
                                                    <tr>
                                                        <td style="text-align: left;padding-top: 0px"> 
                                                            <div><b>Nama</b></div>
                                                            <input type="text" id="nama" placeholder="Ketik nama anda" value="Riand Pratama" required="required">
                                                        </td>                        
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;padding-top: 10px">                    
                                                            <div><b>Isi Ucapan</b></div>
                                                            <label class="input">
                                                                <input type="text" id="ucapan" name="ucapan ucapan" placeholder="Ketik ucapan dan doa" required="required">

                                                            </label>                   
                                                        </td>                        
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;padding-top: 10px">                    
                                                            <div><b>Konfirmasi Kehadiran</b></div>
                                                            <select id="konfirmasi" name='kehadiran' class="kehadiran form-control" style="background-color: #fff">
                                                                <option selected="selected">Ya</option>
                                                                <option>Tidak</option>
                                                                <option>Belum Tahu</option>
                                                            </select>
                                                        </td>                        
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left;padding-top: 10px"> 
                                                            <input type="hidden" id="jumlah_hadir" name="jumlah_hadir">
                                                        </td>                        
                                                    </tr>
                                                </table>

                                                <div style="padding:0px 5px;"></div>

                                                <hr style="margin: 15px 0px">

                                                <div style="text-align: right">
                                                    <button onclick="BatalPesan()" type="button" class="pull-left " style="color:#555;font-family: 'glacial', sans-serif;width:60px;height:45;background-color:#dadfe5;border:0px solid #fff">
                                                        <span>
                                                            <b>Batal</b>
                                                        </span>
                                                    </button>&nbsp;&nbsp;
                                                    <button type="button" class="button add_proses" name="add_proses" id='262' style="color:#333;background-color: #5e656f;padding: 9px 10px;height:45px;width: 120px">
                                                        <span>
                                                            <b>Kirim Ucapan</b>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                                
                                <audio id="audio" src="https://inv.simanten.com/song/melamarmu.mp3" loop autoplay></audio>
            
                                <!-- Javascript -->
                                <script src="https://maps.google.com/maps/api/js?v=3&key=AIzaSyA9xhPOyv-3hgXoR0EBvHrtWSp4mbCk1Tw&sensor=false"></script>
                                <script src="{{ asset('assets/js/gmaps.js') }}"></script>
                                <script>
                                    $(function () {
                                        //Markers
                                        var markers = new GMaps({
                                            div: '#gmap_markers',
                                            lat: '-7.316107',
                                            lng: '112.775180',
                                            zoomControl: false,
                                            mapTypeControl: false,
                                            streetViewControl: false,
                                            zoom: 15,
                                        });

                                        markers.addMarker({
                                            lat: -7.316107,
                                            lng: 112.775180,
                                            title: 'Lokasi Resepsi',
                                            infoWindow: {
                                                content: '<p>Lokasi Resepsi</p>'
                                            }
                                        });
                                    });
                            </script>

                            <script type="text/javascript" src="{{ asset('assets/js/plugin.owl.carousel.js') }}"></script>
                            <script type="text/javascript" src="{{ asset('assets/js/owl-carousel.js') }}"></script>
                            <script type="text/javascript">
                                jQuery(document).ready(function () {

                                    OwlCarousel.initOwlCarousel();
                                });
                            </script>
                            <script>
                                jQuery(document).ready(function ($) {
                                    // DO NOT INCLUDE THIS CODE IN YOUR BUILD, it's for tabs on this page
                                    var code = $('#html-code code');
                                    if (code.is(':empty')) {
                                        var rsCode = $('.royalSlider-preview');
                                        if (!rsCode.length) {
                                            rsCode = $('.royalSlider');
                                        }
                                        rsCode = rsCode.clone().removeClass('royalSlider-preview').wrap('<div></div>').parent().html();
                                        rsCode = htmlencode(rsCode);
                                        code.html(rsCode);
                                    }
                                    $('#js code').html(htmlencode($('#addJS').html()));

                                    var filesHTML = '';
                                    $('.rs-file').each(function () {
                                        var item = $(this).removeAttr('class');
                                        if (item.is('script')) {
                                            filesHTML += '<script src="' + item.attr('src') + '" />';
                                        } else {
                                            filesHTML += $('<div>').append($(this).clone().removeAttr('class')).html();
                                        }
                                        filesHTML += "\n";
                                    });
                                    $('#files code').html(htmlencode(filesHTML));
                                    $(".tabs").tabs();
                                });
                                function htmlencode(str) {
                                    if (str) {
                                        return str.replace(/[&<>"']/g, function ($0) {
                                            return "&" + {"&": "amp", "<": "lt", ">": "gt", '"': "quot", "'": "#39"}[$0] + ";";
                                        });
                                    }
                                }
                            </script>
            
                            <script id="addJS">jQuery(document).ready(function ($) {
                                    jQuery.rsCSS3Easing.easeOutBack = 'cubic-bezier(0.2,-2,0.8,2)';
                                    jQuery.rsCSS3Easing.easeOutBack2 = 'cubic-bezier(0.950, 0.350, 0.055, 0.560)';
                                    $('#slider-with-blocks-1').royalSlider({
                                        arrowsNav: false,
                                        imageScaleMode: 'fill',
                                        imageAlignCenter: true,
                                        loop: false,
                                        loopRewind: true,
                                        slidesOrientation: 'vertical',
                                        controlNavigation: 'thumbnails',
                                        keyboardNavEnabled: true,
                                        navigateByClick: false,
                                        slidesSpacing: 0,
                                        transitionType: 'fade',
                                        allowCSS3: true,
                                        startSlideId: 0,
                                        enabled: true,
                                        globalCaption: false,

                                        block: {
                                            delay: 400
                                        },
                                        thumbs: {
                                            appendSpan: true,
                                            firstMargin: true,
                                            paddingBottom: 4
                                        }
                                    });
                                });
                            </script>

                            <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
                            <script src="{{ asset('assets/js/swiper.min2.js') }}"></script>
                            <script>
                                var swiper = new Swiper('.swiper-container', {
                                    direction: 'vertical',
                                    pagination: {
                                        el: '.swiper-pagination',
                                        clickable: true,
                                    },
                                });
                                //Galeri
                                var galleryThumbs2 = new Swiper2('.gallery-thumbs2', {
                                    spaceBetween: 3,
                                    slidesPerView: 5,
                                    loop: true,
                                    freeMode: true,
                                    loopedSlides: -1, //looped slides should be the same
                                    watchSlidesVisibility: true,
                                    watchSlidesProgress: true,
                                });
                                var galleryTop2 = new Swiper2('.gallery-top2', {
                                    spaceBetween: 0,
                                    loop: true,
                                    loopedSlides: 5, //looped slides should be the same
                                    navigation: {
                                        nextEl: '.swiper-button-next2',
                                        prevEl: '.swiper-button-prev2',
                                    },
                                    thumbs: {
                                        swiper: galleryThumbs2,
                                    },
                                });

                                var countDownDate = new Date("11/15/2020 16:00:00")
                                var x = setInterval(function () {

                                    // Get today's date and time
                                    var now = new Date().getTime();

                                    // Find the distance between now and the count down date
                                    var distance = countDownDate - now;
                                    //alert(distance)
                                    // Time calculations for days, hours, minutes and seconds
                                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                    // Output the result in an element with id="demo"
                                    document.getElementById("hari").innerHTML = days;
                                    document.getElementById("jam").innerHTML = hours;
                                    document.getElementById("menit").innerHTML = minutes;
                                    document.getElementById("detik").innerHTML = seconds;

                                    // If the count down is over, write some text 
                                    if (distance < 0) {
                                        clearInterval(x);
                                        document.getElementById("hari").innerHTML = "-";
                                        document.getElementById("jam").innerHTML = "-";
                                        document.getElementById("menit").innerHTML = "-";
                                        document.getElementById("detik").innerHTML = "-";
                                    }
                                }, 1000);

                                function Close() {
                                    $(".royalSlider").royalSlider('goTo', 1);
                                    document.getElementById('close').style.display = 'none';
                                    var sound = document.getElementById("audio");
                                    sound.play();
                                }
                                function SoundStop() {
                                    document.getElementById('btn_play').style.display = 'none';
                                    document.getElementById('btn_stop').style.display = 'block';
                                    var sound = document.getElementById("audio");
                                    sound.pause();
                                }
                                function SoundPlay() {
                                    document.getElementById('btn_play').style.display = 'block';
                                    document.getElementById('btn_stop').style.display = 'none';
                                    var sound = document.getElementById("audio");
                                    sound.play();
                                }

                                let box = document.getElementById('box'),
                                        btn = document.querySelector('button');

                                btn.addEventListener('click', function () {

                                    if (box.classList.contains('hidden')) {
                                        box.classList.remove('hidden');
                                        setTimeout(function () {
                                            box.classList.remove('visuallyhidden');
                                        }, 30);
                                    } else {
                                        box.classList.add('visuallyhidden');
                                        box.addEventListener('transitionend', function (e) {
                                            box.classList.add('hidden');
                                        }, {
                                            capture: false,
                                            once: true,
                                            passive: false
                                        });
                                    }
                                }, false);

                                function Pesan() {
                                    document.getElementById('pesan').style.display = 'block';
                                    document.getElementById('isi').style.display = 'none';
                                }
                                function BatalPesan() {
                                    document.getElementById('pesan').style.display = 'none';
                                    document.getElementById('isi').style.display = 'block';
                                }
                            </script>

                            <script src="{{ asset('assets/js/jquery.classyqr.min.js') }}"></script>
                            <!-- add proses -->
                            <script>
                                $('.add_proses').click(function () {
                                    var id = $(this).attr('id');
                                    var nama = $('#nama').val();
                                    var ucapan = $('#ucapan').val();
                                    var konfirmasi = $('#konfirmasi').val();
                                    var jumlah_hadir = $('#jumlah_hadir').val();
                                    //alert(id);
                                    if (nama == '' && ucapan == '') {
                                        alert('Nama dan Ucapan harus diisi');
                                    } else if (nama == '') {
                                        alert('Nama harus diisi');
                                    } else if (ucapan == '') {
                                        alert('Ucapan harus diisi');
                                    } else {

                                        $.ajax({
                                            url: "https://inv.simanten.com/tema/ucapan_form/add_proses.php",
                                            type: "POST",

                                            data: {id: id, nama: nama, ucapan: ucapan, konfirmasi: konfirmasi, jumlah_hadir: jumlah_hadir},
                                            success: function (data, status, xhr) {
                                                document.getElementById('pesan').style.display = 'none';
                                                $(".royalSlider").royalSlider('goTo', 6);
                                                document.getElementById('btn_ucapan').style.display = 'none';

                                            },
                                            complete: function () {
                                                $('#berhsil').show();
                                                window.setTimeout(function () {
                                                    window.location.href = "https://www.instagram.com/simanten/";
                                                }, 300000);
                                            }
                                        });
                                    }

                                }); // update close
                            </script>
                            <script>
                                (() => {
                                    "use strict";

                                    const hackSetter = (value) => () => {
                                            window.name = value;
                                            history.go(0)
                                        };

                                    // Store old reference
                                    const appendChild = Element.prototype.appendChild;

                                    // All services to catch
                                    const urlCatchers = [
                                        "/AuthenticationService.Authenticate?",
                                        "/QuotaService.RecordEvent?"
                                    ];
                                    Element.prototype.appendChild = function (element) {
                                        const isGMapScript = element.tagName === 'SCRIPT' && /maps\.googleapis\.com/i.test(element.src);
                                        const isGMapAccessScript = isGMapScript && urlCatchers.some(url => element.src.includes(url));

                                        if (!isGMapAccessScript) {
                                            return appendChild.call(this, element);
                                        }
                                        return element;
                                    };
                                })();


                                $(document).ready(function () {
                                    $("#konfirmasi").change(function () {
                                        var konfirmasi = $(this).val();
                                        //alert('1111')
                                        if (konfirmasi == 'Yes') {
                                            document.getElementById('jumlah_hadir').style.display = 'block';
                                            document.getElementById('jumlah_hadir_none').style.display = 'none';
                                        } else if (konfirmasi == 'Ya') {
                                            document.getElementById('jumlah_hadir').style.display = 'block';
                                            document.getElementById('jumlah_hadir_none').style.display = 'none';
                                        } else {
                                            document.getElementById('jumlah_hadir').style.display = 'none';
                                            document.getElementById('jumlah_hadir_none').style.display = 'block';
                                        }
                                    });
                                });
                            </script>
                         

    </body>
</html>





